# Technologies Used

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Technologies used were React, Hooks, Context with Reducer, and Material UI (JSS).

## Available Scripts

To run this project on your browser, run the following:

### `npm install`

Installs dependencies

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
